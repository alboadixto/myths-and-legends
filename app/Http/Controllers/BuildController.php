<?php

namespace App\Http\Controllers;

use App\Models\Datacard;
use Illuminate\Http\Request;

class BuildController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $search = "";
        if($request->search){
            $search = $request->search;
        }

        $cartas = Datacard::where('name_card', 'LIKE', '%'.$search.'%')
            ->orWhere('set_edition', 'LIKE', '%'.$search.'%')
            ->orWhere('number_card', 'LIKE', '%'.$search.'%')
            ->orWhere('type_card', 'LIKE', '%'.$search.'%')
            ->orWhere('frecuencia', 'LIKE', '%'.$search.'%')
            ->orWhere('Affiliation', 'LIKE', '%'.$search.'%')
            ->orWhere('skill', 'LIKE', '%'.$search.'%')
            ->paginate(12);

        return view('auth.build.index', compact('cartas', 'search'));
    }
}
