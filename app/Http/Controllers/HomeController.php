<?php

namespace App\Http\Controllers;

use App\Models\Datacard;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    public function index()
    {
        $ediciones = Datacard::distinct()->select('set_edition')->get();
        $tipos = Datacard::distinct()->select('type_card')->get();
        $frecuencias = Datacard::distinct()->select('frecuencia')->get();

        return view('home', compact('ediciones', 'tipos', 'frecuencias'));
    }

    public function datatable()
    {
        $dataCard = Datacard::select(['id_card', 'number_card', 'ruta_img', 'skill', 'name_card', 'set_edition', 'type_card', 'frecuencia', 'Affiliation', 'offense', 'cost']);

        return DataTables::of($dataCard)
            ->addColumn('img-card', 'img-card')
            ->addColumn('ability', 'ability')
            ->addColumn('frequency', 'frequency')
            ->rawColumns(['img-card', 'ability', 'frequency'])
            ->make();
    }
}
