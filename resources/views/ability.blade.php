@php
    $nombre = "Permitido en todos los formatos";
    $array = array();

    $formatos = DB::table('formatos')
        ->select('formatos.nombre')
        ->join('relacion_formatos', 'formatos.id_formato', 'relacion_formatos.id_formato')
        ->where('relacion_formatos.id_card', $id_card)
        ->get();

    foreach ($formatos as $key => $formato) {
        $array[] = "$formato->nombre";
    }

    if($array){
        $params = implode(' - ', $array);
        $nombre = $params;
    }
@endphp

<div>
    <p class="text-primary">{{$skill}}</p>
    <span class="badge badge-success">
      {{$nombre}}
    </span>
</div>
