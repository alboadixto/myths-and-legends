@extends('adminlte::page')

@section('title', 'Cartas')

@section('content_header')
{{--  <h1>Cartas</h1> --}}
@stop

@section('content')

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Crear Mazo</h3>
        </div>

        <div class="card-body p-2">
            <form action="{{route('build')}}" method="GET">
                <div class="input-group col-md-6 mt-4 mb-4" style="margin:auto;">
                    <input class="form-control py-2" type="search" placeholder="Buscar" name="search" value="{{$search}}">
                    <span class="input-group-append">
                        <button class="btn btn-outline-primary" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>

            <div class="row">
                @forelse($cartas as $carta)
                    <div class="col-md-3 col-xl-2 mb-3">
                        <img class="mg-rounded mx-auto d-block" src="{{asset('storage/'.$carta->ruta_img)}}" alt="{{$carta->name_card}}" style="width: 200px;">
                    </div>
                @empty
                    <div class="col-12 mb-2">
                        <div class="alert alert-info alert-dismissible">
                            <h5><i class="icon fas fa-exclamation-triangle"></i> No se encontraron resultados...</h5>
                        </div>
                    </div>
                @endforelse
            </div>
            {{$cartas->appends(['search' => $search])->links("pagination::bootstrap-4")}}
        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>

        $(document).ready(function() {

        })

    </script>
@stop
