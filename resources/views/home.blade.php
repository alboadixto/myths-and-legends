@extends('adminlte::page')

@section('title', 'Cartas')

@section('content_header')
{{--     <h1>Cartas</h1> --}}
@stop

@section('content')
    <!--  Datatable -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Cartas</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">

                    <div class="mb-4 callout callout-info">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <input type="text" class="form-control" id="cod" placeholder="Cod.">
                                </div>
                                <div class="col-12 col-md-3">
                                    <input type="text" class="form-control" id="nombre" placeholder="Nombre">
                                </div>
                                <div class="col-12 col-md-3">
                                    <select class="form-control" id="edicion">
                                        <option value="">Seleccione Edición</option>
                                        @foreach ($ediciones as $edicion)
                                            <option value="{{ $edicion->set_edition }}"> {{ $edicion->set_edition }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 col-md-3">
                                    <select class="form-control" id="tipo">
                                        <option value="">Seleccione Tipo</option>
                                        @foreach ($tipos as $tipo)
                                            <option value="{{ $tipo->type_card }}"> {{ $tipo->type_card }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <select class="form-control" id="frecuencia">
                                        <option value="">Seleccione Frecuencia</option>
                                        @foreach ($frecuencias as $frecuencia)
                                            <option value="{{ $frecuencia->frecuencia }}"> {{ $frecuencia->frecuencia }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 col-md-3">
                                    <input type="text" class="form-control" id="raza" placeholder="Raza">
                                </div>
                                <div class="col-12 col-md-3">
                                    <input type="text" class="form-control" id="fuerza" placeholder="Fuerza">
                                </div>
                                <div class="col-12 col-md-3">
                                    <input type="text" class="form-control" id="coste" placeholder="Coste">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <button class="btn btn-block btn-warning" onclick="limpiar()">Limpiar</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table class="table table-hover table-sm" id="tabla-data">
                            <thead>
                                <tr>
                                    <th style="white-space:nowrap; width:50px;">Cod.</th>
                                    <th>Carta</th>
                                    <th>Habilidad</th>
                                    <th>Nombre</th>
                                    <th>Edición</th>
                                    <th>Tipo</th>
                                    <th>Frecuencia</th>
                                    <th>Raza</th>
                                    <th>Fuerza</th>
                                    <th>Coste</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.Datatable-->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>

        function limpiar(){
            document.getElementById("cod").value = "";
            document.getElementById("nombre").value = "";
            document.getElementById("edicion").value = "";
            document.getElementById("tipo").value = "";
            document.getElementById("frecuencia").value = "";
            document.getElementById("raza").value = "";
            document.getElementById("fuerza").value = "";
            document.getElementById("coste").value = "";
            table.
                columns([0,3,4,5,6,7,8,9])
                .search("")
                .draw();
        }

        $('#tabla-data').on('mouseenter', '.zoom', function() {
            $(this).addClass('transition')
        })
        $('#tabla-data').on('mouseleave', '.zoom', function() {
            $(this).removeClass('transition')
        })


        var table = $("#tabla-data").DataTable({
            "responsive": true,
            "autoWidth": false,
            "language": {
                "sProcessing":     '<i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Cargando...</div>',
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "bLengthChange": false,
            "bInfo": false,
            "ordering": false,
            "processing": true,
            "serverSide": true,
            //"order": [[ 1, "desc" ]],
            "ajax":{
                url : "{{ route('datatable') }}",
                type: "GET",
            },
            "columns": [
                {data: 'number_card', name: 'number_card'},
                {data: 'img-card'},
                {data: 'ability', name: 'skill'},
                {data: 'name_card', name: 'name_card'},
                {data: 'set_edition', name: 'set_edition'},
                {data: 'type_card', name: 'type_card'},
                {data: 'frequency', name: 'frecuencia'},
                {data: 'Affiliation', name: 'Affiliation'},
                {data: 'offense', name: 'offense'},
                {data: 'cost', name: 'cost'},
            ],
            "columnDefs": [
                { "searchable": false, "targets": 1 },
            ]
        });

        $("#cod").on('keyup', function(e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                table.
                columns([0])
                .search($(this).val())
                .draw();
            }
        });

        $("#nombre").on('keyup', function(e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                table.
                columns([3])
                .search($(this).val())
                .draw();
            }
        });

        $('#edicion').on('change', function(){
            table.
                columns([4])
                .search($(this).val())
                .draw();
        });

        $('#tipo').on('change', function(){
            table.
                columns([5])
                .search($(this).val())
                .draw();
        });

        $('#frecuencia').on('change', function(){
            console.log($(this).val());
            table.
                columns([6])
                .search($(this).val())
                .draw();
        });

        $("#raza").on('keyup', function(e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                table.
                columns([7])
                .search($(this).val())
                .draw();
            }
        });

        $("#fuerza").on('keyup', function(e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                table.
                columns([8])
                .search($(this).val())
                .draw();
            }
        });

        $("#coste").on('keyup', function(e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                table.
                columns([9])
                .search($(this).val())
                .draw();
            }
        });

    </script>
@stop
