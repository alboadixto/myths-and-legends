@php
    if($frecuencia === 'Real'){
        $color = "yellow";
    }elseif($frecuencia === 'Mega Real'){
        $color = "white";
    }elseif($frecuencia === 'Ultra Real'){
        $color = "black";
    }elseif($frecuencia === 'Promocional'){
        $color = "green";
    }elseif($frecuencia === 'Legendaria'){
        $color = "brown";
    }elseif($frecuencia === 'Cortesano'){
        $color = "blue";
    }elseif($frecuencia === 'Vasallo'){
        $color = "red";
    }else{
        $color = "purple";
    }
@endphp

<div>
    <p>{{$type_card}}</p>
    <span class="{{$color}}">
      {{$frecuencia}}
    </span>
</div>

